# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Added

### Changed


## [0.1.1] - 2016-08-21

### Added

### Changed

- Make every field public


## [0.1.0] - 2016-08-21

Initial commit.


[Unreleased]: https://github.com/zeyla/urbandictionary.rs/compare/v0.1.1...HEAD
[0.1.1]: https://github.com/zeyla/urbandictionary.rs/compare/v0.1.0...v0.1.1
